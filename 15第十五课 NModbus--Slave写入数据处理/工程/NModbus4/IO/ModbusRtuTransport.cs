namespace Modbus.IO
{
    using System;
    using System.IO;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;

    using Message;
    using Utility;

    /// <summary>
    /// 串行MODBUS RTU传输对象类，
    /// 这里在设计模式上用到了桥接模式，参考 http://en.wikipedia.org/wiki/Bridge_Pattern
    /// </summary>
    internal class ModbusRtuTransport : ModbusSerialTransport
    {
        /// <summary>
        /// 请求帧开始的7个字节
        /// </summary>
        public const int RequestFrameStartLength = 7;
        /// <summary>
        /// 响应帧开始的4个字节
        /// </summary>
        public const int ResponseFrameStartLength = 4;


        /// <summary>
        /// 初始化RTU传输类对象实例。
        /// </summary>
        /// <param name="streamResource">数据流资源对象</param>
        internal ModbusRtuTransport(IStreamResource streamResource)
            : base(streamResource)
        {
            Debug.Assert(streamResource != null, "Argument streamResource cannot be null.");
        }

        /// <summary>
        /// 获取请求消息中要读取的字节长度
        /// </summary>
        /// <param name="frameStart">请求数据包</param>
        /// <returns>要读取字节长度</returns>
        public static int RequestBytesToRead(byte[] frameStart)
        {
            byte functionCode = frameStart[1];
            int numBytes;

            switch (functionCode)
            {
                case Modbus.ReadCoils:
                case Modbus.ReadInputs:
                case Modbus.ReadHoldingRegisters:
                case Modbus.ReadInputRegisters:
                case Modbus.WriteSingleCoil:
                case Modbus.WriteSingleRegister:
                case Modbus.Diagnostics:
                    numBytes = 1;
                    break;
                case Modbus.WriteMultipleCoils:
                case Modbus.WriteMultipleRegisters:
                    byte byteCount = frameStart[6];
                    numBytes = byteCount + 2;
                    break;
                default:
                    string errorMessage = String.Format(CultureInfo.InvariantCulture, "Function code {0} not supported.",
                        functionCode);
                    Debug.WriteLine(errorMessage);
                    throw new NotImplementedException(errorMessage);
            }

            return numBytes;
        }

        /// <summary>
        /// 获取响应包需要读取的字节长度
        /// </summary>
        /// <param name="frameStart">响应数据包</param>
        /// <returns>要读取字节长度</returns>
        public static int ResponseBytesToRead(byte[] frameStart)
        {
            byte functionCode = frameStart[1];

            // exception response
            if (functionCode > Modbus.ExceptionOffset)
                return 1;

            int numBytes;
            switch (functionCode)
            {
                case Modbus.ReadCoils:
                case Modbus.ReadInputs:
                case Modbus.ReadHoldingRegisters:
                case Modbus.ReadInputRegisters:
                    numBytes = frameStart[2] + 1;
                    break;
                case Modbus.WriteSingleCoil:
                case Modbus.WriteSingleRegister:
                case Modbus.WriteMultipleCoils:
                case Modbus.WriteMultipleRegisters:
                case Modbus.Diagnostics:
                    numBytes = 4;
                    break;
                default:
                    string errorMessage = String.Format(CultureInfo.InvariantCulture, "Function code {0} not supported.",
                        functionCode);
                    Debug.WriteLine(errorMessage);
                    throw new NotImplementedException(errorMessage);
            }

            return numBytes;
        }

        /// <summary>
        /// 读取指定长度数据
        /// </summary>
        /// <param name="count">要读取的长度</param>
        /// <returns>字节数组</returns>
        public virtual byte[] Read(int count)
        {
            byte[] frameBytes = new byte[count];
            int numBytesRead = 0;

            while (numBytesRead != count)
                numBytesRead += StreamResource.Read(frameBytes, numBytesRead, count - numBytesRead);

            return frameBytes;
        }

        /// <summary>
        /// 构建完整的数据帧
        /// </summary>
        /// <param name="message">modbus消息</param>
        /// <returns>完整的帧字节数组</returns>
        internal override byte[] BuildMessageFrame(IModbusMessage message)
        {
            var messageFrame = message.MessageFrame;
            var crc = ModbusUtility.CalculateCrc(messageFrame);
            var messageBody = new MemoryStream(messageFrame.Length + crc.Length);

            messageBody.Write(messageFrame, 0, messageFrame.Length);
            messageBody.Write(crc, 0, crc.Length);

            return messageBody.ToArray();
        }

        /// <summary>
        /// 校验CRC
        /// </summary>
        /// <param name="message">modbus消息</param>
        /// <param name="messageFrame">消息帧字节数组</param>
        /// <returns>是否校验通过，是返回true，否返回false</returns>
        internal override bool ChecksumsMatch(IModbusMessage message, byte[] messageFrame)
        {
            return BitConverter.ToUInt16(messageFrame, messageFrame.Length - 2) ==
                   BitConverter.ToUInt16(ModbusUtility.CalculateCrc(message.MessageFrame), 0);
        }

        /// <summary>
        /// 读取特定modbus消息响应
        /// </summary>
        /// <typeparam name="T">modbus消息</typeparam>
        /// <returns>返回T对象的响应消息</returns>
        internal override IModbusMessage ReadResponse<T>()
        {
            byte[] frameStart = Read(ResponseFrameStartLength);
            byte[] frameEnd = Read(ResponseBytesToRead(frameStart));
            byte[] frame = Enumerable.Concat(frameStart, frameEnd).ToArray();
            Debug.WriteLine("RX: {0}", string.Join(", ", frame));

            return CreateResponse<T>(frame);
        }

        /// <summary>
        /// 读请求数据帧
        /// </summary>
        /// <returns>返回消息数据帧</returns>
        internal override byte[] ReadRequest()
        {
            byte[] frameStart = Read(RequestFrameStartLength);
            byte[] frameEnd = Read(RequestBytesToRead(frameStart));
            byte[] frame = Enumerable.Concat(frameStart, frameEnd).ToArray();
            Debug.WriteLine("RX: {0}", string.Join(", ", frame));

            return frame;
        }
    }
}
