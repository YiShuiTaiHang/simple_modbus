namespace Modbus.Data
{
    using System;
    using System.IO;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Net;

    using Utility;

    /// <summary>
    ///  16位寄存器的集合。包括保持寄存器和输入寄存器数据.
    /// </summary>
    public class RegisterCollection : Collection<ushort>, IModbusMessageDataCollection
    {
        /// <summary>
        /// 初始化<参见cref="RegisterCollection" />类的一个新实例。
        /// </summary>
        public RegisterCollection()
        {
        }

        /// <summary>
        /// 初始化<see cref="RegisterCollection" />类的一个新实例。
        /// </summary>
        /// <param name="bytes">字节数组</param>
        public RegisterCollection(byte[] bytes)
            : this((IList<ushort>) ModbusUtility.NetworkBytesToHostUInt16(bytes))
        {
        }

        /// <summary>
        /// 初始化<see cref="RegisterCollection" />类的一个新实例。
        /// </summary>
        /// <param name="registers">ushort数组</param>
        public RegisterCollection(params ushort[] registers)
            : this((IList<ushort>) registers)
        {
        }

        /// <summary>
        /// 初始化<see cref="RegisterCollection" />类的一个新实例。
        /// </summary>
        /// <param name="registers">ushort列表</param>
        public RegisterCollection(IList<ushort> registers)
            : base(registers.IsReadOnly ? new List<ushort>(registers) : registers)
        {
        }

        /// <summary>
        ///  获取网络字节数组。
        /// </summary>
        public byte[] NetworkBytes
        {
            get
            {
                var bytes = new MemoryStream(ByteCount);

                foreach (ushort register in this)
                {
                    var b = BitConverter.GetBytes((ushort)IPAddress.HostToNetworkOrder((short)register));
                    bytes.Write(b, 0, b.Length);
                }

                return bytes.ToArray();
            }
        }

        /// <summary>
        ///  获取字节数。
        /// </summary>
        public byte ByteCount
        {
            get { return (byte) (Count*2); }
        }

        /// <summary>
        ///  将当前对象转字符串
        /// </summary>
        /// <returns>
        ///  字符串
        /// </returns>
        public override string ToString()
        {
            return String.Concat("{", String.Join(", ", this.Select(v => v.ToString()).ToArray()), "}");
        }
    }
}
