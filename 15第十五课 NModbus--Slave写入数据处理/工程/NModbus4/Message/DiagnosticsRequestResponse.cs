namespace Modbus.Message
{
    using System;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.Linq;
    using System.Net;

    using Data;

    using Unme.Common;

    /// <summary>
    /// 诊断功能请求、响应类。
    /// </summary>
    internal class DiagnosticsRequestResponse : AbstractModbusMessageWithData<RegisterCollection>, IModbusMessage
    {
        /// <summary>
        /// 初始化诊断功能请求、响应实例。
        /// </summary>
        public DiagnosticsRequestResponse()
        {
        }

        /// <summary>
        /// 使用(子功能码，从设备地址，数据集合)，初始化诊断功能请求、响应实例。
        /// </summary>
        /// <param name="subFunctionCode">子功能码</param>
        /// <param name="slaveAddress">从设备地址</param>
        /// <param name="data">数据集合</param>
        public DiagnosticsRequestResponse(ushort subFunctionCode, byte slaveAddress, RegisterCollection data)
            : base(slaveAddress, Modbus.Diagnostics)
        {
            SubFunctionCode = subFunctionCode;
            Data = data;
        }

        /// <summary>
        /// 帧最小长度。
        /// </summary>
        public override int MinimumFrameSize
        {
            get { return 6; }
        }

        /// <summary>
        /// 子功能码。
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode",
            Justification = "May implement addtional sub function codes in the future.")]
        public ushort SubFunctionCode
        {
            get { return MessageImpl.SubFunctionCode.Value; }
            set { MessageImpl.SubFunctionCode = value; }
        }

        /// <summary>
        /// 消息转字符串。
        /// </summary>
        /// <returns>返回消息字符串</returns>
        public override string ToString()
        {
            Debug.Assert(SubFunctionCode == Modbus.DiagnosticsReturnQueryData,
                "Need to add support for additional sub-function.");

            return String.Format(CultureInfo.InvariantCulture,
                "Diagnostics message, sub-function return query data - {0}.", Data);
        }

        /// <summary>
        /// 特殊初始化
        /// </summary>
        /// <param name="frame">帧字节数组</param>
        protected override void InitializeUnique(byte[] frame)
        {
            SubFunctionCode = (ushort) IPAddress.NetworkToHostOrder(BitConverter.ToInt16(frame, 2));
            Data = new RegisterCollection(frame.Slice(4, 2).ToArray());
        }
    }
}
