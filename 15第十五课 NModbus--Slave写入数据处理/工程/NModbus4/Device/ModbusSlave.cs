namespace Modbus.Device
{
    using System;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.Linq;

    using Data;
    using IO;
    using Message;

    using Unme.Common;

    /// <summary>
    /// Modbus从设备。
    /// </summary>
    public abstract class ModbusSlave : ModbusDevice
    {
        /// <summary>
        /// 初始化modbus从设备
        /// </summary>
        /// <param name="unitId">传输ID</param>
        /// <param name="transport">传输对象</param>
        internal ModbusSlave(byte unitId, ModbusTransport transport)
            : base(transport)
        {
            DataStore = DataStoreFactory.CreateDefaultDataStore();
            UnitId = unitId;
        }

        /// <summary>
        /// 当Modbus slave在处理请求函数之前收到请求时将被引发事件。
        /// </summary>
        /// <exception cref="InvalidModbusRequestException">Modbus请求无效，应该发送指定异常的错误响应。</exception>
        public event EventHandler<ModbusSlaveRequestEventArgs> ModbusSlaveRequestReceived;

        /// <summary>
        ///  当Modbus slave在处理完函数的写部分后收到写请求时将被引发事件。
        /// </summary>
        /// <remarks>对于读/写多个寄存器(函数代码23)，此方法在写入之后和读取之前触发。</remarks>
        public event EventHandler<ModbusSlaveRequestEventArgs> WriteComplete;

        /// <summary>
        /// 获取或设置数据存储区。
        /// </summary>
        public DataStore DataStore { get; set; }

        /// <summary>
        /// 获取或设置传输单元ID。
        /// </summary>
        public byte UnitId { get; set; }

        /// <summary>
        /// 启动从设备侦听请求。
        /// </summary>
        public abstract void Listen();

        /// <summary>
        /// 读离散数据。
        /// </summary>
        /// <param name="request">读线圈、离散输入请求</param>
        /// <param name="dataStore">数据存储</param>
        /// <param name="dataSource">数据源</param>
        /// <returns>读线圈、离散输入响应</returns>
        internal static ReadCoilsInputsResponse ReadDiscretes(ReadCoilsInputsRequest request, DataStore dataStore,
            ModbusDataCollection<bool> dataSource)
        {
            DiscreteCollection data = DataStore.ReadData<DiscreteCollection, bool>(dataStore, dataSource,
                request.StartAddress, request.NumberOfPoints, dataStore.SyncRoot);
            var response = new ReadCoilsInputsResponse(request.FunctionCode, request.SlaveAddress, data.ByteCount, data);

            return response;
        }
        /// <summary>
        /// 读寄存器数据。
        /// </summary>
        /// <param name="request">读保持、输入寄存器请求</param>
        /// <param name="dataStore">数据存储</param>
        /// <param name="dataSource">数据源</param>
        /// <returns>读保持、输入寄存器响应</returns>
        internal static ReadHoldingInputRegistersResponse ReadRegisters(ReadHoldingInputRegistersRequest request,
            DataStore dataStore, ModbusDataCollection<ushort> dataSource)
        {
            RegisterCollection data = DataStore.ReadData<RegisterCollection, ushort>(dataStore, dataSource,
                request.StartAddress, request.NumberOfPoints, dataStore.SyncRoot);
            var response = new ReadHoldingInputRegistersResponse(request.FunctionCode, request.SlaveAddress, data);

            return response;
        }

        /// <summary>
        /// 写单个线圈
        /// </summary>
        /// <param name="request">写单个线圈请求响应消息</param>
        /// <param name="dataStore">数据存储</param>
        /// <param name="dataSource">数据源</param>
        /// <returns>写单个线圈请求响应消息</returns>
        internal static WriteSingleCoilRequestResponse WriteSingleCoil(WriteSingleCoilRequestResponse request,
            DataStore dataStore, ModbusDataCollection<bool> dataSource)
        {
            DataStore.WriteData(dataStore, new DiscreteCollection(request.Data[0] == Modbus.CoilOn), dataSource,
                request.StartAddress, dataStore.SyncRoot);

            return request;
        }

        /// <summary>
        /// 写多个线圈数据。
        /// </summary>
        /// <param name="request">写多个线圈请求</param>
        /// <param name="dataStore">数据存储</param>
        /// <param name="dataSource">数据源</param>
        /// <returns>写多个线圈响应</returns>
        internal static WriteMultipleCoilsResponse WriteMultipleCoils(WriteMultipleCoilsRequest request,
            DataStore dataStore, ModbusDataCollection<bool> dataSource)
        {
            DataStore.WriteData(dataStore, request.Data.Take(request.NumberOfPoints), dataSource, request.StartAddress,
                dataStore.SyncRoot);
            var response = new WriteMultipleCoilsResponse(request.SlaveAddress, request.StartAddress, request.NumberOfPoints);

            return response;
        }
        /// <summary>
        /// 写单个寄存器数据。
        /// </summary>
        /// <param name="request">写单个寄存器请求响应消息</param>
        /// <param name="dataStore">数据存储</param>
        /// <param name="dataSource">数据源</param>
        /// <returns>写单个寄存器请求响应消息</returns>
        internal static WriteSingleRegisterRequestResponse WriteSingleRegister(
            WriteSingleRegisterRequestResponse request, DataStore dataStore, ModbusDataCollection<ushort> dataSource)
        {
            DataStore.WriteData(dataStore, request.Data, dataSource, request.StartAddress, dataStore.SyncRoot);

            return request;
        }
        /// <summary>
        /// 写多少寄存器数据。
        /// </summary>
        /// <param name="request">写多个寄存器请求</param>
        /// <param name="dataStore">数据存储</param>
        /// <param name="dataSource">数据源</param>
        /// <returns>写多个寄存器响应</returns>
        internal static WriteMultipleRegistersResponse WriteMultipleRegisters(WriteMultipleRegistersRequest request,
            DataStore dataStore, ModbusDataCollection<ushort> dataSource)
        {
            DataStore.WriteData(dataStore, request.Data, dataSource, request.StartAddress, dataStore.SyncRoot);
            var response = new WriteMultipleRegistersResponse(request.SlaveAddress, request.StartAddress, request.NumberOfPoints);

            return response;
        }

        /// <summary>
        /// 应用请求
        /// </summary>
        /// <param name="request">modbus请求消息</param>
        /// <returns></returns>
        [SuppressMessage("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily",
            Justification = "Cast is not unneccessary.")]
        internal IModbusMessage ApplyRequest(IModbusMessage request)
        {
            IModbusMessage response;
            try
            {
                Debug.WriteLine(request.ToString());
                var eventArgs = new ModbusSlaveRequestEventArgs(request);
                ModbusSlaveRequestReceived.Raise(this, eventArgs);

                switch (request.FunctionCode)
                {
                    case Modbus.ReadCoils:
                        response = ReadDiscretes((ReadCoilsInputsRequest)request, DataStore, DataStore.CoilDiscretes);
                        break;
                    case Modbus.ReadInputs:
                        response = ReadDiscretes((ReadCoilsInputsRequest)request, DataStore, DataStore.InputDiscretes);
                        break;
                    case Modbus.ReadHoldingRegisters:
                        response = ReadRegisters((ReadHoldingInputRegistersRequest)request, DataStore,
                            DataStore.HoldingRegisters);
                        break;
                    case Modbus.ReadInputRegisters:
                        response = ReadRegisters((ReadHoldingInputRegistersRequest)request, DataStore,
                            DataStore.InputRegisters);
                        break;
                    case Modbus.Diagnostics:
                        response = request;
                        break;
                    case Modbus.WriteSingleCoil:
                        response = WriteSingleCoil((WriteSingleCoilRequestResponse)request, DataStore,
                            DataStore.CoilDiscretes);
                        WriteComplete.Raise(this, eventArgs);
                        break;
                    case Modbus.WriteSingleRegister:
                        response = WriteSingleRegister((WriteSingleRegisterRequestResponse)request, DataStore,
                            DataStore.HoldingRegisters);
                        WriteComplete.Raise(this, eventArgs);
                        break;
                    case Modbus.WriteMultipleCoils:
                        response = WriteMultipleCoils((WriteMultipleCoilsRequest)request, DataStore,
                            DataStore.CoilDiscretes);
                        WriteComplete.Raise(this, eventArgs);
                        break;
                    case Modbus.WriteMultipleRegisters:
                        response = WriteMultipleRegisters((WriteMultipleRegistersRequest)request, DataStore,
                            DataStore.HoldingRegisters);
                        WriteComplete.Raise(this, eventArgs);
                        break;
                    case Modbus.ReadWriteMultipleRegisters:
                        ReadWriteMultipleRegistersRequest readWriteRequest = (ReadWriteMultipleRegistersRequest)request;
                        WriteMultipleRegisters(readWriteRequest.WriteRequest, DataStore, DataStore.HoldingRegisters);
                        WriteComplete.Raise(this, eventArgs);
                        response = ReadRegisters(readWriteRequest.ReadRequest, DataStore, DataStore.HoldingRegisters);
                        break;
                    default:
                        string errorMessage = String.Format(CultureInfo.InvariantCulture, "Unsupported function code {0}",
                            request.FunctionCode);
                        Debug.WriteLine(errorMessage);
                        throw new InvalidModbusRequestException(Modbus.IllegalFunction);
                }
            }
            catch (InvalidModbusRequestException ex) // Catches the exception for an illegal function or a custom exception from the ModbusSlaveRequestReceived event.
            {
                response = new SlaveExceptionResponse(request.SlaveAddress, (byte)(Modbus.ExceptionOffset + request.FunctionCode), ex.ExceptionCode);
            }

            return response;
        }
    }
}
