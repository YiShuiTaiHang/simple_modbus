namespace Modbus.Message
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;

    /// <summary>
    /// 
    /// </summary>
    public class SlaveExceptionResponse : AbstractModbusMessage, IModbusMessage
    {
        private static readonly Dictionary<byte, string> _exceptionMessages = CreateExceptionMessages();

        /// <summary>
        /// 
        /// </summary>
        public SlaveExceptionResponse()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="slaveAddress"></param>
        /// <param name="functionCode"></param>
        /// <param name="exceptionCode"></param>
        public SlaveExceptionResponse(byte slaveAddress, byte functionCode, byte exceptionCode)
            : base(slaveAddress, functionCode)
        {
            SlaveExceptionCode = exceptionCode;
        }

        /// <summary>
        /// 
        /// </summary>
        public override int MinimumFrameSize
        {
            get { return 3; }
        }

        /// <summary>
        /// 
        /// </summary>
        public byte SlaveExceptionCode
        {
            get { return MessageImpl.ExceptionCode.Value; }
            set { MessageImpl.ExceptionCode = value; }
        }

        /// <summary>
        ///     Returns a <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </summary>
        /// <returns>
        ///     A <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </returns>
        public override string ToString()
        {
            string message = _exceptionMessages.ContainsKey(SlaveExceptionCode)
                ? _exceptionMessages[SlaveExceptionCode]
                : ModbusResources.Unknown;
            return String.Format(CultureInfo.InvariantCulture, ModbusResources.SlaveExceptionResponseFormat,
                Environment.NewLine, FunctionCode, SlaveExceptionCode, message);
        }

        internal static Dictionary<byte, string> CreateExceptionMessages()
        {
            Dictionary<byte, string> messages = new Dictionary<byte, string>(9);

            messages.Add(1, ModbusResources.IllegalFunction);
            messages.Add(2, ModbusResources.IllegalDataAddress);
            messages.Add(3, ModbusResources.IllegalDataValue);
            messages.Add(4, ModbusResources.SlaveDeviceFailure);
            messages.Add(5, ModbusResources.Acknowlege);
            messages.Add(6, ModbusResources.SlaveDeviceBusy);
            messages.Add(8, ModbusResources.MemoryParityError);
            messages.Add(10, ModbusResources.GatewayPathUnavailable);
            messages.Add(11, ModbusResources.GatewayTargetDeviceFailedToRespond);

            return messages;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="frame"></param>
        protected override void InitializeUnique(byte[] frame)
        {
            if (FunctionCode <= Modbus.ExceptionOffset)
                throw new FormatException(ModbusResources.SlaveExceptionResponseInvalidFunctionCode);

            SlaveExceptionCode = frame[2];
        }
    }
}
