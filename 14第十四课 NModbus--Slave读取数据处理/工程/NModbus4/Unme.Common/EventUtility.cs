﻿namespace Modbus.Unme.Common
{
    using System;
    /// <summary>
    /// 事件实用程序
    /// </summary>
    internal static class EventUtility
    {
        /// <summary>
        /// 引发事件
        /// </summary>
        /// <param name="handler">需要触发的事件</param>
        /// <param name="sender">事件参数</param>
        public static void Raise(this EventHandler handler, object sender)
        {
            if (handler == null)
                return;

            handler(sender, EventArgs.Empty);
        }

        /// <summary>
        /// 引发事件
        /// </summary>
        /// <typeparam name="T">事件的参数类型</typeparam>
        /// <param name="handler">需要触发的带参数事件</param>
        /// <param name="sender">事件参数</param>
        /// <param name="e">泛型事件参数</param>
        public static void Raise<T>(this EventHandler<T> handler, object sender, T e) where T : EventArgs
        {
            if (handler == null)
                return;

            handler(sender, e);
        }
    }
}
