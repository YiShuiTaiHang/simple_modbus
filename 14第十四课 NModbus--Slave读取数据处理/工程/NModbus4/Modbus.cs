namespace Modbus
{
    /// <summary>
    /// 主要是定义了modbus协议相关的常量。
    /// </summary>
    internal static class Modbus
    {
        #region supported function codes
        /// <summary>
        /// 读线圈功能码
        /// </summary>
        public const byte ReadCoils = 1;
        /// <summary>
        /// 读离散输入功能码
        /// </summary>
        public const byte ReadInputs = 2;
        /// <summary>
        /// 读保持寄存器功能码
        /// </summary>
        public const byte ReadHoldingRegisters = 3;
        /// <summary>
        /// 读输入寄存器功能码
        /// </summary>
        public const byte ReadInputRegisters = 4;
        /// <summary>
        /// 写单个线圈功能码
        /// </summary>
        public const byte WriteSingleCoil = 5;
        /// <summary>
        /// 写单个寄存器功能码
        /// </summary>
        public const byte WriteSingleRegister = 6;
        /// <summary>
        /// 诊断功能码
        /// </summary>
        public const byte Diagnostics = 8;
        /// <summary>
        /// 诊断返回查询数据子功能码
        /// </summary>
        public const ushort DiagnosticsReturnQueryData = 0;
        /// <summary>
        /// 写多个线圈功能码
        /// </summary>
        public const byte WriteMultipleCoils = 15;
        /// <summary>
        /// 写多个寄存器功能码
        /// </summary>
        public const byte WriteMultipleRegisters = 16;
        /// <summary>
        /// 读/写多个寄存器功能码
        /// </summary>
        public const byte ReadWriteMultipleRegisters = 23;
        #endregion

        /// <summary>
        /// 离散量请求和返回数量的最大值
        /// </summary>
        public const int MaximumDiscreteRequestResponseSize = 2040;

        /// <summary>
        /// 寄存器请求和返回数量的最大值
        /// </summary>
        public const int MaximumRegisterRequestResponseSize = 127;

        /// <summary>
        /// Modbus从设备异常偏移量，以标记异常
        /// </summary>
        public const byte ExceptionOffset = 128;

        #region modbus slave exception codes
        /// <summary>
        /// 无效功能码标识
        /// </summary>
        public const byte IllegalFunction = 1;
        /// <summary>
        /// 无效数据地址标识
        /// </summary>
        public const byte IllegalDataAddress = 2;
        /// <summary>
        /// 应答标识
        /// </summary>
        public const byte Acknowledge = 5;
        /// <summary>
        /// 从设备设备忙标识
        /// </summary>
        public const byte SlaveDeviceBusy = 6;
        #endregion

        /// <summary>
        /// 重试IO操作次数的默认设置
        /// </summary>
        public const int DefaultRetries = 3;

        /// <summary>
        /// 遇到应答或从设备忙碌的从设备异常响应后，等待的毫秒数。
        /// </summary>
        public const int DefaultWaitToRetryMilliseconds = 250;

        /// <summary>
        /// IO超时的默认设置为毫秒
        /// </summary>
        public const int DefaultTimeout = 1000;

        /// <summary>
        /// 最小的支持消息帧大小（SANS校验和）
        /// </summary>
        public const int MinimumFrameSize = 2;

        /// <summary>
        /// 线圈开
        /// </summary>
        public const ushort CoilOn = 0xFF00;
        /// <summary>
        /// 线圈关
        /// </summary>
        public const ushort CoilOff = 0x0000;

        /// <summary>
        /// IP从设备的地址由Ip决定，默认是0
        /// </summary>
        public const byte DefaultIpSlaveUnitId = 0;

        /// <summary>
        /// 远程主机强制关闭现有连接错误码
        /// </summary>
        public const int ConnectionResetByPeer = 10054;

        /// <summary>
        /// 现有的套接字连接正在关闭错误码
        /// </summary>
        public const int WSACancelBlockingCall = 10004;

        /// <summary>
        /// 由ASCII传输用于表示消息结束
        /// </summary>
        public const string NewLine = "\r\n";
    }
}
