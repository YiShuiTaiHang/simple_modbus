namespace Modbus.Device
{
    using IO;

    /// <summary>
    ///  Modbus串行主设备接口。
    /// </summary>
    public interface IModbusSerialMaster : IModbusMaster
    {
        /// <summary>
        ///  主设备需要使用的传输对象。
        /// </summary>
        new ModbusSerialTransport Transport { get; }

        /// <summary>
        ///     只支持串行线路。诊断函数，它循环回原始数据。
        ///     NModbus只支持回循环一个ushort值，这是RTU协议
        ///     的“Best Effort”实现的一个限制。
        /// </summary>
        /// <param name="slaveAddress">从设备地址</param>
        /// <param name="data">返回的数据</param>
        /// <returns>如果从设备回显数据，返回true。</returns>
        bool ReturnQueryData(byte slaveAddress, ushort data);
    }
}
