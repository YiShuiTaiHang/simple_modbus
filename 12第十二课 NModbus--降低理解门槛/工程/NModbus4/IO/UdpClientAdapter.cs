﻿namespace Modbus.IO
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Sockets;
    using System.Threading;

    using Unme.Common;

    /// <summary>
    /// UDP客户端适配器，实现<see cref="IStreamResource"/>接口，
    /// 这里在设计模式上用到了桥接模式，参考 http://en.wikipedia.org/wiki/Bridge_Pattern
    /// </summary>
    internal class UdpClientAdapter : IStreamResource
    {
        /// <summary>
        /// UDP客户端。
        /// </summary>
        private UdpClient _udpClient;
        /// <summary>
        /// 字节换成列表。
        /// </summary>
        private List<byte> Buffer;

        /// <summary>
        /// 初始化UDP客户端适配器实例。
        /// </summary>
        /// <param name="udpClient">UDP客户端</param>
        public UdpClientAdapter(UdpClient udpClient)
        {
            if (udpClient == null)
                throw new ArgumentNullException("udpClient");

            _udpClient = udpClient;
        }

        /// <summary>
        /// 无限延时。
        /// </summary>
        public int InfiniteTimeout
        {
            get { return Timeout.Infinite; }
        }

        /// <summary>
        /// 读数据超时。
        /// </summary>
        public int ReadTimeout
        {
            get { return _udpClient.Client.ReceiveTimeout; }
            set { _udpClient.Client.ReceiveTimeout = value; }
        }

        /// <summary>
        /// 写数据超时。
        /// </summary>
        public int WriteTimeout
        {
            get { return _udpClient.Client.SendTimeout; }
            set { _udpClient.Client.SendTimeout = value; }
        }

        /// <summary>
        /// 缓冲区中丢弃
        /// </summary>
        public void DiscardInBuffer()
        {
            // no-op
        }

        /// <summary>
        /// 读数据。
        /// </summary>
        /// <param name="buffer">字节数组</param>
        /// <param name="offset">偏移量</param>
        /// <param name="count">数量</param>
        /// <returns>读取数量</returns>
        public int Read(byte[] buffer, int offset, int count)
        {
            if (buffer == null)
                throw new ArgumentNullException("buffer");
            if (offset < 0)
                throw new ArgumentOutOfRangeException("offset", "Argument offset must be greater than or equal to 0.");
            if (offset > buffer.Length)
                throw new ArgumentOutOfRangeException("offset", "Argument offset cannot be greater than the length of buffer.");
            if (count < 0)
                throw new ArgumentOutOfRangeException("count", "Argument count must be greater than or equal to 0.");
            if (count > buffer.Length - offset)
                throw new ArgumentOutOfRangeException("count", "Argument count cannot be greater than the length of buffer minus offset.");

            if (Buffer == null || Buffer.Count == 0)
            {
                IPEndPoint remoteIpEndPoint = null;
                Buffer = _udpClient.Receive(ref remoteIpEndPoint).ToList();
            }

            if (Buffer.Count < count)
                throw new IOException("Not enough bytes in the datagram.");

            Buffer.CopyTo(0, buffer, offset, count);
            Buffer.RemoveRange(0, count);

            return count;
        }

        /// <summary>
        /// 写数据。
        /// </summary>
        /// <param name="buffer">字节数组</param>
        /// <param name="offset">偏移量</param>
        /// <param name="count">数量</param>
        public void Write(byte[] buffer, int offset, int count)
        {
            if (buffer == null)
                throw new ArgumentNullException("buffer");
            if (offset < 0)
                throw new ArgumentOutOfRangeException("offset", "Argument offset must be greater than or equal to 0.");
            if (offset > buffer.Length)
                throw new ArgumentOutOfRangeException("offset", "Argument offset cannot be greater than the length of buffer.");
            if (count < 0)
                throw new ArgumentOutOfRangeException("count", "Argument count must be greater than or equal to 0.");
            if (count > buffer.Length - offset)
                throw new ArgumentOutOfRangeException("count", "Argument count cannot be greater than the length of buffer minus offset.");

            _udpClient.Send(buffer.Skip(offset).ToArray(), count);
        }

        /// <summary>
        /// 资源释放。
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        /// <summary>
        /// 资源释放。
        /// </summary>
        /// <param name="disposing">是否在释放中</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                DisposableUtility.Dispose(ref _udpClient);
        }
    }
}
