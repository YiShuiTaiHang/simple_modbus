using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Modbus.Message;

namespace Modbus
{
    /// <summary>
    /// 通信期间发生的从设备错误类。
    /// </summary>
    [Serializable]
    public class SlaveException : Exception
    {
        /// <summary>
        /// 从设备异常相应
        /// </summary>
        private readonly SlaveExceptionResponse _slaveExceptionResponse;

        /// <summary>
        /// 从设备地址属性名
        /// </summary>
        private const string SlaveAddressPropertyName = "SlaveAdress";
        /// <summary>
        /// 功能码属性名
        /// </summary>
        private const string FunctionCodePropertyName = "FunctionCode";
        /// <summary>
        /// 从设备异常码属性名
        /// </summary>
        private const string SlaveExceptionCodePropertyName = "SlaveExceptionCode";

        /// <summary>
        /// 初始化 <see cref="SlaveException" />类的新实例。
        /// </summary>
        public SlaveException()
        {
        }

        /// <summary>
        /// 初始化 <see cref="SlaveException" />类的新实例。
        /// </summary>
        /// <param name="message">相关消息</param>
        public SlaveException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// 初始化 <see cref="SlaveException" />类的新实例。
        /// </summary>
        /// <param name="message">相关消息</param>
        /// <param name="innerException">内部异常</param>
        public SlaveException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// 初始化 <see cref="SlaveException" />类的新实例.
        /// </summary>
        /// <param name="slaveExceptionResponse">从设备异常响应<see cref="SlaveExceptionResponse"/>类</param>
        internal SlaveException(SlaveExceptionResponse slaveExceptionResponse)
        {
            _slaveExceptionResponse = slaveExceptionResponse;
        }

        /// <summary>
        /// 初始化 <see cref="SlaveException" />类的新实例。
        /// </summary>
        /// <param name="message">相关消息</param>
        /// <param name="slaveExceptionResponse">从设备异常响应<see cref="SlaveExceptionResponse"/>类</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode",
            Justification = "Used by test code.")]
        internal SlaveException(string message, SlaveExceptionResponse slaveExceptionResponse)
            : base(message)
        {
            _slaveExceptionResponse = slaveExceptionResponse;
        }

        /// <summary>
        ///     初始化 <see cref="SlaveException" />类的新实例。
        /// </summary>
        /// <param name="info">
        ///     <see cref="T:System.Runtime.Serialization.SerializationInfo"></see> 
        ///     包含关于被抛出异常的序列化对象数据。
        /// </param>
        /// <param name="context">
        ///     <see cref="T:System.Runtime.Serialization.StreamingContext"></see> 
        ///     包含有关源或目标的上下文信息。
        /// </param>
        /// <exception cref="T:System.Runtime.Serialization.SerializationException">
        ///     类名称是空或者
        ///     <see cref="P:System.Exception.HResult"></see>是0。
        /// </exception>
        /// <exception cref="T:System.ArgumentNullException">Info参数是空</exception>
        protected SlaveException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            if (info != null)
            {
                _slaveExceptionResponse = new SlaveExceptionResponse(info.GetByte(SlaveAddressPropertyName),
                    info.GetByte(FunctionCodePropertyName), info.GetByte(SlaveExceptionCodePropertyName));
            }
        }

        /// <summary>
        /// 获取描述当前异常的消息。
        /// </summary>
        /// <value></value>
        /// <returns>解释异常的原因或空字符串（“”）的错误消息。</returns>
        public override string Message
        {
            get
            {
                return String.Concat(base.Message,
                    _slaveExceptionResponse != null
                        ? String.Concat(Environment.NewLine, _slaveExceptionResponse)
                        : String.Empty);
            }
        }

        /// <summary>
        /// 获取导致异常发生异常的响应函数代码或0。
        /// </summary>
        /// <value>The function code.</value>
        public byte FunctionCode
        {
            get { return _slaveExceptionResponse != null ? _slaveExceptionResponse.FunctionCode : (byte) 0; }
        }

        /// <summary>
        ///  获取从异常代码，或0。
        /// </summary>
        /// <value>从异常代码。</value>
        public byte SlaveExceptionCode
        {
            get { return _slaveExceptionResponse != null ? _slaveExceptionResponse.SlaveExceptionCode : (byte) 0; }
        }

        /// <summary>
        /// 获取从地址，或0。
        /// </summary>
        /// <value>从设备地址</value>
        public byte SlaveAddress
        {
            get { return _slaveExceptionResponse != null ? _slaveExceptionResponse.SlaveAddress : (byte) 0; }
        }

        /// <summary>
        /// 在派生类中重写时，设置 <see cref="T:System.Runtime.Serialization.SerializationInfo"></see>
        /// 带有关于异常的信息。
        /// </summary>
        /// <param name="info">
        ///  <see cref="T:System.Runtime.Serialization.SerializationInfo"></see>
        ///  保存有关正在抛出的异常的序列化对象数据。
        /// </param>
        /// <param name="context">
        ///   <see cref="T:System.Runtime.Serialization.StreamingContext"></see> 
        ///   包含关于源或目标的上下文信息。
        /// </param>
        /// <exception cref="T:System.ArgumentNullException">info参数是一个空引用(在Visual Basic中没有任何内容)。</exception>
        /// <PermissionSet>
        ///     <IPermission
        ///         class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089"
        ///         version="1" Read="*AllFiles*" PathDiscovery="*AllFiles*" />
        ///     <IPermission
        ///         class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089"
        ///         version="1" Flags="SerializationFormatter" />
        /// </PermissionSet>
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        [SuppressMessage("Microsoft.Design", "CA1062:ValidateArgumentsOfPublicMethods",
            Justification = "Argument info is validated, rule does not understand AND condition.")]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);

            if (info != null && _slaveExceptionResponse != null)
            {
                info.AddValue(SlaveAddressPropertyName, _slaveExceptionResponse.SlaveAddress);
                info.AddValue(FunctionCodePropertyName, _slaveExceptionResponse.FunctionCode);
                info.AddValue(SlaveExceptionCodePropertyName, _slaveExceptionResponse.SlaveExceptionCode);
            }
        }
    }
}
