namespace Modbus.Message
{
    using System;
    using System.IO;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Net;

    using Data;

    /// <summary>
    ///   保存两个或多个消息类型之间共享的所有实现。
    ///   接口暴露了类型特定实现的子集。
    /// </summary>
    internal class ModbusMessageImpl
    {
        /// <summary>
        /// 初始化modbusbus消息实例。
        /// </summary>
        public ModbusMessageImpl()
        {
        }

        /// <summary>
        /// 使用(从设备地址，功能码)，初始化modbus消息实例。
        /// </summary>
        /// <param name="slaveAddress"></param>
        /// <param name="functionCode"></param>
        public ModbusMessageImpl(byte slaveAddress, byte functionCode)
        {
            SlaveAddress = slaveAddress;
            FunctionCode = functionCode;
        }

        /// <summary>
        /// 字节数。
        /// </summary>
        public byte? ByteCount { get; set; }

        /// <summary>
        /// 异常码。
        /// </summary>
        public byte? ExceptionCode { get; set; }

        /// <summary>
        /// 传输帧ID编码。
        /// </summary>
        public ushort TransactionId { get; set; }

        /// <summary>
        /// 功能码。
        /// </summary>
        public byte FunctionCode { get; set; }

        /// <summary>
        /// 数据数量。
        /// </summary>
        public ushort? NumberOfPoints { get; set; }

        /// <summary>
        /// 从设备地址。
        /// </summary>
        public byte SlaveAddress { get; set; }

        /// <summary>
        /// 起始地址。
        /// </summary>
        public ushort? StartAddress { get; set; }

        /// <summary>
        /// 子功能码。
        /// </summary>
        public ushort? SubFunctionCode { get; set; }

        /// <summary>
        /// 带数据集合的Modbus消息
        /// </summary>
        public IModbusMessageDataCollection Data { get; set; }

        /// <summary>
        /// 数据帧数组。
        /// </summary>
        public byte[] MessageFrame
        {
            get
            {
                var pdu = ProtocolDataUnit;
                var frame = new MemoryStream(1 + pdu.Length);

                frame.WriteByte(SlaveAddress);
                frame.Write(pdu, 0, pdu.Length);

                return frame.ToArray();
            }
        }

        /// <summary>
        /// 协议数据单元（PDU）。
        /// </summary>
        public byte[] ProtocolDataUnit
        {
            get
            {
                List<byte> pdu = new List<byte>();

                pdu.Add(FunctionCode);

                if (ExceptionCode.HasValue)
                    pdu.Add(ExceptionCode.Value);

                if (SubFunctionCode.HasValue)
                    pdu.AddRange(BitConverter.GetBytes(IPAddress.HostToNetworkOrder((short) SubFunctionCode.Value)));

                if (StartAddress.HasValue)
                    pdu.AddRange(BitConverter.GetBytes(IPAddress.HostToNetworkOrder((short) StartAddress.Value)));

                if (NumberOfPoints.HasValue)
                    pdu.AddRange(BitConverter.GetBytes(IPAddress.HostToNetworkOrder((short) NumberOfPoints.Value)));

                if (ByteCount.HasValue)
                    pdu.Add(ByteCount.Value);

                if (Data != null)
                    pdu.AddRange(Data.NetworkBytes);

                return pdu.ToArray();
            }
        }

        /// <summary>
        /// 初始化指令帧。
        /// </summary>
        /// <param name="frame">帧字节数组</param>
        public void Initialize(byte[] frame)
        {
            if (frame == null)
                throw new ArgumentNullException("frame", "Argument frame cannot be null.");

            if (frame.Length < Modbus.MinimumFrameSize)
                throw new FormatException(String.Format(CultureInfo.InvariantCulture,
                    "Message frame must contain at least {0} bytes of data.", Modbus.MinimumFrameSize));

            SlaveAddress = frame[0];
            FunctionCode = frame[1];
        }
    }
}
