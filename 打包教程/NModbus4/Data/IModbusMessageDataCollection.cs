namespace Modbus.Data
{
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    ///  包含数据的Modbus消息。
    /// </summary>
    [SuppressMessage("Microsoft.Naming", "CA1711:IdentifiersShouldNotHaveIncorrectSuffix")]
    public interface IModbusMessageDataCollection
    {
        /// <summary>
        /// 获取网络字节。
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays")]
        byte[] NetworkBytes { get; }

        /// <summary>
        ///  获取字节计数。
        /// </summary>
        byte ByteCount { get; }
    }
}
