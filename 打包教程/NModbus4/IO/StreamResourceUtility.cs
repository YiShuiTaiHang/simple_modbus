﻿namespace Modbus.IO
{
    using System.Linq;
    using System.Text;

    /// <summary>
    /// 数据流资源实用程序
    /// </summary>
    internal static class StreamResourceUtility
    {
        /// <summary>
        /// 读取数据流资源
        /// </summary>
        /// <param name="stream">数据流资源对象</param>
        /// <returns>数据流字符串</returns>
        internal static string ReadLine(IStreamResource stream)
        {
            var result = new StringBuilder();
            var singleByteBuffer = new byte[1];

            do
            {
                if (0 == stream.Read(singleByteBuffer, 0, 1))
                    continue;

                result.Append(Encoding.ASCII.GetChars(singleByteBuffer).First());
            } while (!result.ToString().EndsWith(Modbus.NewLine));

            return result.ToString().Substring(0, result.Length - Modbus.NewLine.Length);
        }
    }
}
