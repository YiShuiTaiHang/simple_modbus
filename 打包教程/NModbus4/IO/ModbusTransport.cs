namespace Modbus.IO
{
    using System;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Threading;

    using Message;

    using Unme.Common;

    /// <summary>
    /// modbus消息传输对象类。
    /// 这里在设计模式上用到了桥接模式，参考 http://en.wikipedia.org/wiki/Bridge_Pattern
    /// </summary>
    public abstract class ModbusTransport : IDisposable
    {
        /// <summary>
        /// 同步锁:
        /// 保证锁定的操作归属当前线程，其他线程被阻塞，直到该对象被释放。
        /// 参考 https://docs.microsoft.com/zh-cn/dotnet/csharp/language-reference/keywords/lock-statement
        /// </summary>
        private readonly object _syncLock = new object();
        /// <summary>
        /// modbus消息写入从设备重试次数，默认3次。
        /// </summary>
        private int _retries = Modbus.DefaultRetries;
        /// <summary>
        /// 重试时间间隔，默认250ms。
        /// </summary>
        private int _waitToRetryMilliseconds = Modbus.DefaultWaitToRetryMilliseconds;
        /// <summary>
        /// 数据流资源对象。
        /// </summary>
        private IStreamResource _streamResource;

        /// <summary>
        ///  初始化modbus传输对象实例。
        /// </summary>
        internal ModbusTransport()
        {
        }

        /// <summary>
        /// 使用IStreamResource初始化传输对象实例。
        /// </summary>
        /// <param name="streamResource">数据流资源对象</param>
        internal ModbusTransport(IStreamResource streamResource)
        {
            Debug.Assert(streamResource != null, "Argument streamResource cannot be null.");

            _streamResource = streamResource;
        }

        /// <summary>
        /// 重发次数。
        /// </summary>
        public int Retries
        {
            get { return _retries; }
            set { _retries = value; }
        }

        /// <summary>
        /// 如果非零，则如果第一个回复落后于请求的序列号小于此数字，
        /// 则将导致读取第二个回复。例如，将其设置为3，如果在发送
        /// 请求5时读取响应3，我们将尝试重新读取响应。
        /// </summary>
        public uint RetryOnOldResponseThreshold { get; set; }

        /// <summary>
        /// 如果设置，则从忙异常会导致使用重试计数。
        /// 如果为False，则Slave Busy将导致无限次重试。
        /// </summary>
        public bool SlaveBusyUsesRetryCount { get; set; }

        /// <summary>
        /// 获取或设置在收到确认或从属设备忙从异常响应后传输在重
        /// 试消息之前等待的毫秒数。
        /// </summary>
        public int WaitToRetryMilliseconds
        {
            get { return _waitToRetryMilliseconds; }
            set
            {
                if (value < 0)
                    throw new ArgumentException(Resources.WaitRetryGreaterThanZero);

                _waitToRetryMilliseconds = value;
            }
        }

        /// <summary>
        ///  获取或设置读取操作未完成时发生超时之前的毫秒数。
        ///  </summary>
        public int ReadTimeout
        {
            get { return StreamResource.ReadTimeout; }
            set { StreamResource.ReadTimeout = value; }
        }

        /// <summary>
        ///  获取或设置写入操作未完成时发生超时之前的毫秒数。
        /// </summary>
        public int WriteTimeout
        {
            get { return StreamResource.WriteTimeout; }
            set { StreamResource.WriteTimeout = value; }
        }

        /// <summary>
        ///  获取数据流资源。
        /// </summary>
        internal IStreamResource StreamResource
        {
            get { return _streamResource; }
        }

        /// <summary>
        /// 执行与释放、释放或重置非托管资源相关的应用程序定义的任务。
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// 执行单次消息会话。
        /// </summary>
        /// <typeparam name="T">modbus的消息类型</typeparam>
        /// <param name="message">消息对象</param>
        /// <returns>返回对应T类型modbus的消息</returns>
        internal virtual T UnicastMessage<T>(IModbusMessage message) where T : IModbusMessage, new()
        {
            IModbusMessage response = null;
            int attempt = 1;
            bool success = false;

            do
            {
                try
                {
                    lock (_syncLock)
                    {
                        Write(message);

                        bool readAgain;
                        do
                        {
                            readAgain = false;
                            response = ReadResponse<T>();

                            var exceptionResponse = response as SlaveExceptionResponse;
                            if (exceptionResponse != null)
                            {
                                // 如果SlaveExceptionCode==确认，我们将重试读取响应，而不重新提交请求
                                readAgain = exceptionResponse.SlaveExceptionCode == Modbus.Acknowledge;
                                if (readAgain)
                                {
                                    Debug.WriteLine(
                                        "Received ACKNOWLEDGE slave exception response, waiting {0} milliseconds and retrying to read response.",
                                        _waitToRetryMilliseconds);
                                    Sleep(WaitToRetryMilliseconds);
                                }
                                else
                                {
                                    throw new SlaveException(exceptionResponse);
                                }
                            }
                            else if (ShouldRetryResponse(message, response))
                            {
                                readAgain = true;
                            }
                        } while (readAgain);
                    }

                    ValidateResponse(message, response);
                    success = true;
                }
                catch (SlaveException se)
                {
                    if (se.SlaveExceptionCode != Modbus.SlaveDeviceBusy)
                        throw;

                    if (SlaveBusyUsesRetryCount && attempt++ > _retries)
                        throw;

                    Debug.WriteLine(
                        "Received SLAVE_DEVICE_BUSY exception response, waiting {0} milliseconds and resubmitting request.",
                        _waitToRetryMilliseconds);
                    Sleep(WaitToRetryMilliseconds);
                }
                catch (Exception e)
                {
                    if (e is FormatException ||
                        e is NotImplementedException ||
                        e is TimeoutException ||
                        e is IOException)
                    {
                        Debug.WriteLine("{0}, {1} retries remaining - {2}", e.GetType().Name, _retries - attempt + 1, e);

                        if (attempt++ > _retries)
                            throw;
                    }
                    else
                    {
                        throw;
                    }
                }
            } while (!success);

            return (T)response;
        }

        /// <summary>
        /// 创建应答消息。
        /// </summary>
        /// <typeparam name="T">modbus消息类型</typeparam>
        /// <param name="frame">消息帧字节数组</param>
        /// <returns>返回对应消息</returns>
        internal virtual IModbusMessage CreateResponse<T>(byte[] frame) where T : IModbusMessage, new()
        {
            byte functionCode = frame[1];
            IModbusMessage response;

            // 检查从属异常响应，否则从设备帧创建消息
            if (functionCode > Modbus.ExceptionOffset)
                response = ModbusMessageFactory.CreateModbusMessage<SlaveExceptionResponse>(frame);
            else
                response = ModbusMessageFactory.CreateModbusMessage<T>(frame);

            return response;
        }

        /// <summary>
        /// 验证响应的消息。
        /// </summary>
        /// <param name="request">请求的消息对象</param>
        /// <param name="response">响应的消息对象</param>
        internal void ValidateResponse(IModbusMessage request, IModbusMessage response)
        {
            // 无论传输协议如何，始终检查功能代码和从机地址
            if (request.FunctionCode != response.FunctionCode)
                throw new IOException(String.Format(CultureInfo.InvariantCulture,
                    "Received response with unexpected Function Code. Expected {0}, received {1}.", request.FunctionCode,
                    response.FunctionCode));

            if (request.SlaveAddress != response.SlaveAddress)
                throw new IOException(String.Format(CultureInfo.InvariantCulture,
                    "Response slave address does not match request. Expected {0}, received {1}.", response.SlaveAddress,
                    request.SlaveAddress));

            // 特定于消息的验证
            var req = request as IModbusRequest;
            if (req != null)
            {
                req.ValidateResponse(response);
            }

            OnValidateResponse(request, response);
        }

        /// <summary>
        /// 在处理之前检查是否需要尝试读取另一个响应(例如，响应来自上一个请求)。
        /// </summary>
        internal bool ShouldRetryResponse(IModbusMessage request, IModbusMessage response)
        {
            // 这些检查在验证请求中强制执行，我们不想重试这些检查
            if (request.FunctionCode != response.FunctionCode) { return false; }
            if (request.SlaveAddress != response.SlaveAddress) { return false; }
            return OnShouldRetryResponse(request, response); ;
        }

        /// <summary>
        /// 提供钩子以检查是否应该重试接收响应。
        /// </summary>
        internal virtual bool OnShouldRetryResponse(IModbusMessage request, IModbusMessage response)
        {
            return false;
        }

        /// <summary>
        /// 提供钩子来执行传输级消息验证。
        /// </summary>
        internal abstract void OnValidateResponse(IModbusMessage request, IModbusMessage response);

        /// <summary>
        /// 读取请求。
        /// </summary>
        /// <returns>帧字节数组</returns>
        internal abstract byte[] ReadRequest();

        /// <summary>
        /// 读取响应。
        /// </summary>
        /// <typeparam name="T">modbus消息类型</typeparam>
        /// <returns>返回对应消息</returns>
        internal abstract IModbusMessage ReadResponse<T>() where T : IModbusMessage, new();

        /// <summary>
        /// 构建完整消息帧，例如RTU补充CRC数据。
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        internal abstract byte[] BuildMessageFrame(IModbusMessage message);

        /// <summary>
        /// 写入消息数据.
        /// </summary>
        /// <param name="message">modbus 对应消息</param>
        internal abstract void Write(IModbusMessage message);

        /// <summary>
        ///  释放非托管和(可选)托管资源
        /// </summary>
        /// <param name="disposing">
        /// <c>True</c> 释放托管资源和非托管资源；
        /// <c>False</c> 仅释放非托管资源。
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                DisposableUtility.Dispose(ref _streamResource);
        }

        /// <summary>
        /// 睡眠控制
        /// </summary>
        /// <param name="millisecondsTimeout">睡眠时间ms</param>
        private static void Sleep(int millisecondsTimeout)
        {
            Thread.Sleep(millisecondsTimeout);
        }
    }
}
