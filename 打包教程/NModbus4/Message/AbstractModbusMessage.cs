namespace Modbus.Message
{
    using System;
    using System.Globalization;

    /// <summary>
    /// modbus消息抽象类
    /// </summary>
    public abstract class AbstractModbusMessage
    {
        /// <summary>
        /// modbus消息接口类实现对象。
        /// </summary>
        private readonly ModbusMessageImpl _messageImpl;

        /// <summary>
        /// modbus消息实例抽象方法。
        /// </summary>
        internal AbstractModbusMessage()
        {
            _messageImpl = new ModbusMessageImpl();
        }

        /// <summary>
        /// 使用(从设备地址，功能码)的modbus消息实例抽象方法。
        /// </summary>
        /// <param name="slaveAddress"></param>
        /// <param name="functionCode"></param>
        internal AbstractModbusMessage(byte slaveAddress, byte functionCode)
        {
            _messageImpl = new ModbusMessageImpl(slaveAddress, functionCode);
        }

        /// <summary>
        /// 使用IP协议时分配给消息的唯一标识符。
        /// </summary>
        public ushort TransactionId
        {
            get { return _messageImpl.TransactionId; }
            set { _messageImpl.TransactionId = value; }
        }

        /// <summary>
        /// 功能码，告诉服务器要执行的操作。
        /// </summary>
        public byte FunctionCode
        {
            get { return _messageImpl.FunctionCode; }
            set { _messageImpl.FunctionCode = value; }
        }

        /// <summary>
        /// 从设备(服务端)地址。
        /// </summary>
        public byte SlaveAddress
        {
            get { return _messageImpl.SlaveAddress; }
            set { _messageImpl.SlaveAddress = value; }
        }

        /// <summary>
        ///  modbus消息接口类实现对象。
        /// </summary>
        internal ModbusMessageImpl MessageImpl
        {
            get { return _messageImpl; }
        }

        /// <summary>
        /// 由从设备地址和协议数据单元组（PDU）成的消息帧。
        /// </summary>
        public byte[] MessageFrame
        {
            get { return _messageImpl.MessageFrame; }
        }

        /// <summary>
        /// 功能代码和消息数据的组成的协议数据单元（PDU）。
        /// </summary>
        public virtual byte[] ProtocolDataUnit
        {
            get { return _messageImpl.ProtocolDataUnit; }
        }

        /// <summary>
        /// 最小帧长度。
        /// </summary>
        public abstract int MinimumFrameSize { get; }

        /// <summary>
        /// 从指定的消息帧初始化modbus消息。
        /// </summary>
        /// <param name="frame">消息帧</param>
        public void Initialize(byte[] frame)
        {
            if (frame.Length < MinimumFrameSize)
                throw new FormatException(String.Format(CultureInfo.InvariantCulture,
                    "Message frame must contain at least {0} bytes of data.", MinimumFrameSize));

            _messageImpl.Initialize(frame);
            InitializeUnique(frame);
        }

        /// <summary>
        /// 特殊初始化抽象方法，被Initialize调用。
        /// </summary>
        /// <param name="frame"></param>
        protected abstract void InitializeUnique(byte[] frame);
    }
}
