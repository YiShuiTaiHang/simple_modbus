﻿namespace Modbus.Unme.Common
{
    using System;
    /// <summary>
    /// 用完释放实用程序
    /// </summary>
    internal static class DisposableUtility
    {
        /// <summary>
        /// 释放
        /// </summary>
        /// <typeparam name="T">需要传入的数据类型</typeparam>
        /// <param name="item">传入的数据</param>
        public static void Dispose<T>(ref T item) where T : class, IDisposable
        {
            if (item == null)
                return;

            item.Dispose();
            item = default(T);
        }
    }
}
