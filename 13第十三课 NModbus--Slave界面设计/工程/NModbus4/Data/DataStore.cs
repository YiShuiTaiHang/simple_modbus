namespace Modbus.Data
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;

    using Unme.Common;

    /// <summary>
    ///     对象模拟设备内存映射。当使用ModbusMaster API读取/写入值时，
    ///     底层集合是线程安全的。您可以使用SyncRoot属性同步对数据存储集合的直接访问。
    /// </summary>
    public class DataStore
    {
        private readonly object _syncRoot = new object();

        /// <summary>
        /// 对象的新实例<see cref="DataStore" /> .
        /// </summary>
        public DataStore()
        {
            CoilDiscretes = new ModbusDataCollection<bool> {ModbusDataType = ModbusDataType.Coil};
            InputDiscretes = new ModbusDataCollection<bool> {ModbusDataType = ModbusDataType.Input};
            HoldingRegisters = new ModbusDataCollection<ushort> {ModbusDataType = ModbusDataType.HoldingRegister};
            InputRegisters = new ModbusDataCollection<ushort> {ModbusDataType = ModbusDataType.InputRegister};
        }

        /// <summary>
        /// 使用list对象来创建新实例。
        /// </summary>
        /// <param name="coilDiscretes">线圈列表</param>
        /// <param name="inputDiscretes">离散输入列表</param>
        /// <param name="holdingRegisters">保持寄存器列表</param>
        /// <param name="inputRegisters">输入寄存器列表</param>
        internal DataStore(IList<bool> coilDiscretes, IList<bool> inputDiscretes, IList<ushort> holdingRegisters, IList<ushort> inputRegisters)
        {
            CoilDiscretes = new ModbusDataCollection<bool>(coilDiscretes) { ModbusDataType = ModbusDataType.Coil };
            InputDiscretes = new ModbusDataCollection<bool>(inputDiscretes) { ModbusDataType = ModbusDataType.Input };
            HoldingRegisters = new ModbusDataCollection<ushort>(holdingRegisters) { ModbusDataType = ModbusDataType.HoldingRegister };
            InputRegisters = new ModbusDataCollection<ushort>(inputRegisters) { ModbusDataType = ModbusDataType.InputRegister };
        }

        /// <summary>
        /// 通过Modbus命令写入数据存储时发生的事件。
        /// </summary>
        public event EventHandler<DataStoreEventArgs> DataStoreWrittenTo;

        /// <summary>
        /// 当通过Modbus命令读取数据存储时发生的事件。
        /// </summary>
        public event EventHandler<DataStoreEventArgs> DataStoreReadFrom;

        /// <summary>
        ///  获取线圈数据。
        /// </summary>
        public ModbusDataCollection<bool> CoilDiscretes { get; private set; }

        /// <summary>
        ///  获取离散输入数据。
        /// </summary>
        public ModbusDataCollection<bool> InputDiscretes { get; private set; }

        /// <summary>
        ///  获取保持寄存器数据。
        /// </summary>
        public ModbusDataCollection<ushort> HoldingRegisters { get; private set; }

        /// <summary>
        ///  获取输入寄存器数据。
        /// </summary>
        public ModbusDataCollection<ushort> InputRegisters { get; private set; }

        /// <summary>
        /// 可用于同步对数据存储集合的直接访问的对象。
        /// </summary>
        public object SyncRoot
        {
            get { return _syncRoot; }
        }

        /// <summary>
        ///  从集合中检索数据子集。
        /// </summary>
        /// <typeparam name="T">集合类型。</typeparam>
        /// <typeparam name="U">集合中元素的类型。</typeparam>
        internal static T ReadData<T, U>(DataStore dataStore, ModbusDataCollection<U> dataSource, ushort startAddress,
            ushort count, object syncRoot) where T : Collection<U>, new()
        {
            int startIndex = startAddress + 1;

            if (startIndex < 0 || dataSource.Count < startIndex + count)
                throw new InvalidModbusRequestException(Modbus.IllegalDataAddress);

            U[] dataToRetrieve;
            lock (syncRoot)
                dataToRetrieve = dataSource.Slice(startIndex, count).ToArray();

            T result = new T();
            for (int i = 0; i < count; i++)
                result.Add(dataToRetrieve[i]);

            dataStore.DataStoreReadFrom.Raise(dataStore,
                DataStoreEventArgs.CreateDataStoreEventArgs(startAddress, dataSource.ModbusDataType, result));

            return result;
        }

        /// <summary>
        /// 数据写入数据存储。
        /// </summary>
        /// <typeparam name="TData">数据的类型。</typeparam>
        internal static void WriteData<TData>(DataStore dataStore, IEnumerable<TData> items,
            ModbusDataCollection<TData> destination, ushort startAddress, object syncRoot)
        {
            int startIndex = startAddress + 1;

            if (startIndex < 0 || destination.Count < startIndex + items.Count())
                throw new InvalidModbusRequestException(Modbus.IllegalDataAddress);

            lock (syncRoot)
                Update(items, destination, startIndex);

            dataStore.DataStoreWrittenTo.Raise(dataStore,
                DataStoreEventArgs.CreateDataStoreEventArgs(startAddress, destination.ModbusDataType, items));
        }

        /// <summary>
        ///  更新集合中值的子集。
        /// </summary>
        internal static void Update<T>(IEnumerable<T> items, IList<T> destination, int startIndex)
        {
            if (startIndex < 0 || destination.Count < startIndex + items.Count())
                throw new InvalidModbusRequestException(Modbus.IllegalDataAddress);

            int index = startIndex;
            foreach (T item in items)
            {
                destination[index] = item;
                ++index;
            }
        }
    }
}
