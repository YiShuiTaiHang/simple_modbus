﻿namespace Modbus.Message
{
    /// <summary>
    /// Modbus请求消息类，包含特定于请求类的方法。
    /// </summary>
    public interface IModbusRequest : IModbusMessage
    {
        /// <summary>
        /// 根据当前请求验证指定的响应。
        /// </summary>
        /// <param name="response">返回消息</param>
        void ValidateResponse(IModbusMessage response);
    }
}
