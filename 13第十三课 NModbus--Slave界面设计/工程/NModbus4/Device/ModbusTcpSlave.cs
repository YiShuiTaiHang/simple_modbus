namespace Modbus.Device
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Linq;
    using System.Net.Sockets;
    using System.Diagnostics;
    using System.IO;
    using System.Timers;

    using IO;

    /// <summary>
    ///  Modbus TCP从设备。
    /// </summary>
    public class ModbusTcpSlave : ModbusSlave
    {
        /// <summary>
        /// 服务端锁。
        /// </summary>
        private readonly object _serverLock = new object();

        /// <summary>
        /// 主设备字典，存储回话对应信息。
        /// </summary>
        private readonly ConcurrentDictionary<string, ModbusMasterTcpConnection> _masters =
            new ConcurrentDictionary<string, ModbusMasterTcpConnection>();
        /// <summary>
        /// TCP服务端
        /// </summary>
        private TcpListener _server;
        /// <summary>
        /// 定时器
        /// </summary>
        private Timer _timer;
        /// <summary>
        /// 响应等待时间。
        /// </summary>
        private const int TimeWaitResponse = 1000;

        /// <summary>
        /// 初始化modbus TCP从设备实例。
        /// </summary>
        /// <param name="unitId">传输ID</param>
        /// <param name="tcpListener">监听实例</param>
        private ModbusTcpSlave(byte unitId, TcpListener tcpListener)
            : base(unitId, new EmptyTransport())
        {
            if (tcpListener == null)
                throw new ArgumentNullException("tcpListener");

            _server = tcpListener;
        }

        /// <summary>
        /// 初始化modbus TCP从设备实例。
        /// </summary>
        /// <param name="unitId">传输ID</param>
        /// <param name="tcpListener">监听实例</param>
        /// <param name="timeInterval">超时间隔</param>
        private ModbusTcpSlave(byte unitId, TcpListener tcpListener, double timeInterval)
            : base(unitId, new EmptyTransport())
        {
            if (tcpListener == null)
                throw new ArgumentNullException("tcpListener");

            _server = tcpListener;
            _timer = new Timer(timeInterval);
            _timer.Elapsed += OnTimer;
            _timer.Enabled = true;
        }

        /// <summary>
        ///  获取连接到此Modbus TCP Slave的Modbus TCP master。
        /// </summary>
        public ReadOnlyCollection<TcpClient> Masters
        {
            get
            {
                return new ReadOnlyCollection<TcpClient>(_masters.Values.Select(mc => mc.TcpClient).ToList());
            }
        }

        /// <summary>
        /// 获取服务器。
        /// </summary>
        /// <value>服务器实例</value>
        /// <remarks>
        ///  这个属性不是线程安全的，它只能在锁中使用。
        /// </remarks>
        private TcpListener Server
        {
            get
            {
                if (_server == null)
                    throw new ObjectDisposedException("Server");

                return _server;
            }
        }

        /// <summary>
        ///     Modbus TCP 从设备工厂方法.
        /// </summary>
        /// <param name="unitId">传输ID</param>
        /// <param name="tcpListener">监听实例</param>
        /// <returns>从设备实例</returns>
        public static ModbusTcpSlave CreateTcp(byte unitId, TcpListener tcpListener)
        {
            return new ModbusTcpSlave(unitId, tcpListener);
        }

        /// <summary>
        ///  Modbus TCP 从设备工厂方法.每毫秒轮询连接的客户端。
        /// </summary>
        /// <param name="unitId">传输ID</param>
        /// <param name="tcpListener">监听实例</param>
        /// <param name="pollInterval">轮询间隔</param>
        /// <returns>从设备实例</returns>
        public static ModbusTcpSlave CreateTcp(byte unitId, TcpListener tcpListener, double pollInterval)
        {
            return new ModbusTcpSlave(unitId, tcpListener, pollInterval);
        }

        /// <summary>
        /// SOCKET是否已经连接。
        /// </summary>
        /// <param name="socket">socket对象</param>
        /// <returns>连接状态</returns>
        private static bool IsSocketConnected(Socket socket)
        {
            bool poll = socket.Poll(TimeWaitResponse, SelectMode.SelectRead);
            bool available = (socket.Available == 0);
            return poll && available;
        }

        /// <summary>
        ///  启动从侦听请求。
        /// </summary>
        public override void Listen()
        {
            Debug.WriteLine("Start Modbus Tcp Server.");

            lock (_serverLock)
            {
                try
                {
                    Server.Start();

                    // use Socket async API for compact framework compat
                    Server.Server.BeginAccept(state => AcceptCompleted(state), this);
                }
                catch (ObjectDisposedException)
                {
                    // this happens when the server stops
                }
            }
        }

        /// <summary>
        /// 定时事件处理。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnTimer(object sender, ElapsedEventArgs e)
        {
            foreach (var master in _masters.ToList())
            {
                if (IsSocketConnected(master.Value.TcpClient.Client) == false)
                {
                    master.Value.Dispose();
                }
            }
        }

        /// <summary>
        /// 当主设备发生断连接开触发事件。
        /// </summary>
        /// <param name="sender">object对象</param>
        /// <param name="e">tcp事件参数</param>
        private void OnMasterConnectionClosedHandler(object sender, TcpConnectionEventArgs e)
        {
            ModbusMasterTcpConnection connection;
            if (!_masters.TryRemove(e.EndPoint, out connection))
            {
                var msg = string.Format(
                    CultureInfo.InvariantCulture,
                    "EndPoint {0} cannot be removed, it does not exist.",
                    e.EndPoint);

                throw new ArgumentException(msg);
            }

            Debug.WriteLine("Removed Master {0}", e.EndPoint);
        }
        /// <summary>
        /// 主设备接入到从设备完成回调。
        /// </summary>
        /// <param name="ar"></param>
        private static void AcceptCompleted(IAsyncResult ar)
        {
            ModbusTcpSlave slave = (ModbusTcpSlave)ar.AsyncState;

            try
            {
                try
                {
                    // use Socket async API for compact framework compat
                    Socket socket = null;
                    lock (slave._serverLock)
                    {
                        if (slave._server == null) // Checks for disposal to an otherwise unnecessary exception (which is slow and hinders debugging).
                            return;
                        socket = slave.Server.Server.EndAccept(ar);
                    }

                    TcpClient client = new TcpClient {Client = socket};
                    var masterConnection = new ModbusMasterTcpConnection(client, slave);
                    masterConnection.ModbusMasterTcpConnectionClosed += slave.OnMasterConnectionClosedHandler;

                    slave._masters.TryAdd(client.Client.RemoteEndPoint.ToString(), masterConnection);

                    Debug.WriteLine("Accept completed.");
                }
                catch (IOException ex)
                {
                    // Abandon the connection attempt and continue to accepting the next connection.
                    Debug.WriteLine("Accept failed: " + ex.Message);
                }

                // Accept another client
                // use Socket async API for compact framework compat
                lock (slave._serverLock)
                {
                    slave.Server.Server.BeginAccept(state => AcceptCompleted(state), slave);
                }
            }
            catch (ObjectDisposedException)
            {
                // this happens when the server stops
            }
        }

        /// <summary>
        ///  释放非托管资源和(可选)托管资源
        /// </summary>
        /// <param name="disposing">
        ///   true释放托管和非托管资源;false只释放非托管资源。
        /// </param>
        /// <remarks>处理是线程安全的。</remarks>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                // double-check locking
                if (_server != null)
                {
                    lock (_serverLock)
                    {
                        if (_server != null)
                        {
                            _server.Stop();
                            _server = null;

                            if (_timer != null)
                            {
                                _timer.Dispose();
                                _timer = null;
                            }

                            foreach (var key in _masters.Keys)
                            {
                                ModbusMasterTcpConnection connection;
                                if (_masters.TryRemove(key, out connection))
                                {
                                    connection.ModbusMasterTcpConnectionClosed -= OnMasterConnectionClosedHandler;
                                    connection.Dispose();
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
