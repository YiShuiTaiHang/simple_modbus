﻿namespace Modbus.Device
{
    using System;

    /// <summary>
    /// TCP连接事件参数类。
    /// </summary>
    internal class TcpConnectionEventArgs : EventArgs
    {
        /// <summary>
        /// 初始化TCP连接事件参数实例。
        /// </summary>
        /// <param name="endPoint">端口</param>
        public TcpConnectionEventArgs(string endPoint)
        {
            if (endPoint == null)
                throw new ArgumentNullException("endPoint");
            if (endPoint == string.Empty)
                throw new ArgumentException(Resources.EmptyEndPoint);

            EndPoint = endPoint;
        }

        /// <summary>
        /// 设置、获取端口。
        /// </summary>
        public string EndPoint { get; set; }
    }
}
